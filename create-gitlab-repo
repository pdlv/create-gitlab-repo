#!/usr/bin/env python
import argparse
import json
import logging
import os.path
import shutil
import urllib
import urllib2

import xdg.BaseDirectory

from ConfigParser import SafeConfigParser
from subprocess import CalledProcessError, check_call, check_output
try:
    import readline  # noqa
except ImportError:
    pass


logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)

def get_config(section):
    gitlab_config_file = os.path.join(xdg.BaseDirectory.xdg_config_home,
                                      'gitlab')
    conf_parser = SafeConfigParser()
    conf_parser.read(gitlab_config_file)

    if section is not None:
        cur_site = section
    elif conf_parser.has_section('general'):
        cur_site = conf_parser.get('general', 'default')
    else:
        cur_site = conf_parser.sections()[0]

    token = conf_parser.get(cur_site, 'token')
    base_url = conf_parser.get(cur_site, 'base_url')
    return token, base_url

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--server', help='label for a Gitlab server')
arg_parser.add_argument('--name', help='name of the created repository')
args = arg_parser.parse_args()

gitlab_token, gitlab_base_url = get_config(args.server)

curdir = os.path.abspath(os.curdir)
pkgname = os.path.basename(curdir)
clone_repo = '{}.git'.format(pkgname)

# creation of the new repo itself
check_call(['git', 'clone', '--bare', '.', clone_repo])

reponame = args.name.strip() if args.name is not None else pkgname

try:
    check_call('( cd {} ; git gc --auto --prune )'.format(clone_repo),
               shell=True)

    # collect description
    desc = raw_input('Enter a brief description of the project:\n').strip()
    logging.debug('desc = "{}"'.format(desc))
    if len(desc) == 0:
        raise KeyboardInterrupt('Premature termination')

    # move the repo data to the remote server
    # scp -r $PKGNAME.git luther:/srv/git/
    req = urllib2.Request(url='{}/projects/'.format(gitlab_base_url))
    req.add_header('PRIVATE-TOKEN', gitlab_token)
    paras = {
        'name': reponame,
        'descripton': desc,
        'wiki_enabled': 'false',
        'merge_requests_enabled': 'true',
        'issues_enabled': 'true',
        'public': 'true',
    }
    req.add_data(urllib.urlencode(paras))
    logging.debug('req:\n%s', str(req))
    logging.debug('req:\n%s', req.get_full_url())
    logging.debug('req:\n%s', req.get_data())
    logging.debug('req:\n%s', req.header_items())
    ret = urllib2.urlopen(req)

    logging.debug('retcode = %d', ret.getcode())
    if (ret.getcode() / 100) == 2:
        response = json.load(ret)
        logging.debug('response:\n%s', str(response))

        push_url = response['ssh_url_to_repo']
        pull_url = response['http_url_to_repo']
        project_id = response['id']
        logging.debug('project_id = %d' % project_id)

    # set up remotes
    check_call(['git', 'remote', 'add', 'gitlab', pull_url])
    check_call(['git', 'remote', 'set-url', '--push', 'gitlab',
                push_url])
    check_call(['git', 'push', '--all', 'gitlab'])
    check_call(['git', 'push', '--tags', 'gitlab'])

    # Unprotect master branch
    unprotect_url = '%s/projects/%d/repository/branches/master/unprotect' \
        % (gitlab_base_url, project_id,)
    req = urllib2.Request(url=unprotect_url)
    req.get_method = lambda: 'PUT'
    req.add_header('PRIVATE-TOKEN', gitlab_token)
    urllib2.urlopen(req)

    # make the current branch track the remote branch of the same name
    branch = check_output(['git', 'symbolic-ref', 'HEAD']).strip()
    branch = os.path.basename(branch)
    check_call(['git', 'branch', '-u', 'gitlab/{}'.format(branch)])

except CalledProcessError:
    pass

except KeyboardInterrupt:
    pass

finally:
    shutil.rmtree(clone_repo, ignore_errors=True)


shutil.rmtree(clone_repo, ignore_errors=True)
